<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/jdlr/modefinder/controller/ControllerPublic.php');

$controllerPublic = new ControllerPublic();
?>

<header>
  <div>
    <a href="index.php"><h1><?php echo json_decode($controllerPublic->getText("header", NULL, "titre"), true)[0]["texte"]; ?></h1></a>
    <h4><?php echo json_decode($controllerPublic->getText("header", NULL, "soustitre"), true)[0]["texte"]; ?></h4>
  </div>
  <div>
    <img alt="menuBur" id="menuBur" src="<?php echo json_decode($controllerPublic->getImg("header", NULL), true)[0]["link"]; ?>" />
  </div>
