</header>
<nav>
  <a id="presentation" href="frontend/view/presentation.php"><?php echo json_decode($this->getText("nav", NULL, "menu1"), true)[0]["texte"]; ?></a>
  <a id="ressource" href="frontend/view/ressource.php"><?php echo json_decode($this->getText("nav", NULL, "menu2"), true)[0]["texte"]; ?></a>
  <a id="contact" class="NAVcont" href="frontend/view/contact.php"><?php echo json_decode($this->getText("nav", NULL, "menu3"), true)[0]["texte"]; ?></a>
</nav>
