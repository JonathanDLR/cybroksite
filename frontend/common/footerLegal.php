<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/jdlr/modefinder/controller/ControllerPublic.php');

$controllerPublic = new ControllerPublic();
?>

<footer>
  <p id="FOOlink">
    <a id="Afb" href="#"><img alt="facebook" src="<?php echo json_decode($controllerPublic->getImg("footer", "ARTfacebook"), true)[0]["link"]; ?>"/></a>
    <a id="Atwt" href="#"><img alt="twitter" src="<?php echo json_decode($controllerPublic->getImg("footer", "ARTtwitter"), true)[0]["link"]; ?>"/></a>
  </p>
</footer>
