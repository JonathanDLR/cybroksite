<?php
require($_SERVER['DOCUMENT_ROOT'].'/jdlr/modefinder/controller/ControllerPublic.php');
$controllerPublic = new ControllerPublic();
?>

<section id="SEClegal">
  <article>
    <a><?php echo json_decode($controllerPublic->getText("legal", NULL, "corps"), true)[0]["texte"]; ?></p>
  </article>
</section>
