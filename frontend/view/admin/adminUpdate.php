<article id="ARTselect">
  <p>Sélectionner la vue que vous souhaitez modifier: <select name="SELnameView" id="SELnameView"></select></p>
  <p>Sélectionner l'article que vous souhaitez modifier: <select name="SELnameArt" id="SELnameArt"></select></p>
  <p>Sélectionner ce que vous souhaitez modifier: <select name="SELnameType" id="SELnameType"></select><p>
</article>

<article id="ARTmodif">
  <div contenteditable="true">
  </div>
  <p id="ARTmodifRep"></p>
  <button id="BUTmodif">Edit</button>
</article>
