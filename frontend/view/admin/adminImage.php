<article id="ARTselectImg">
  <p>Sélectionner la vue que vous souhaitez modifier: <select name="SELnameView" id="SELnameView"></select></p>
  <p>Sélectionner l'article que vous souhaitez modifier: <select name="SELnameArt" id="SELnameArt"></select></p>
</article>

<article id="ARTmodifImg">
    <div>
        <form name="formImage" id="formImage" method="post" enctype="multipart/form-data">
        <label for="image" id="LABimage">Choose a file</label>
            <input type="file" name="image" id="image"/>
            <input type="submit" value="Send Image" id="SUBimg" />
        </form>
    </div>
  <p id="ARTmodifRepImg"></p>
</article>
