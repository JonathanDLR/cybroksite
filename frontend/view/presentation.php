<?php
require($_SERVER['DOCUMENT_ROOT'].'/jdlr/modefinder/controller/ControllerPublic.php');
$controllerPublic = new ControllerPublic();
?>

<section id="SECpresentation">
  <article id="ARTdescription">
    <p><?php echo json_decode($controllerPublic->getText("presentation", "ARTdescription", "corps"), true)[0]["texte"]; ?></p>
    <img alt="image appli" src="<?php echo json_decode($controllerPublic->getImg("presentation", "ARTdescription"), true)[0]["link"]; ?>"/>
  </article>
  <article id="ARTreconnaissance">
    <p><?php echo json_decode($controllerPublic->getText("presentation", "ARTreconnaissance", "corps"), true)[0]["texte"]; ?></p>
    <img alt="image Reconnaissance" src="<?php echo json_decode($controllerPublic->getImg("presentation", "ARTreconnaissance"), true)[0]["link"]; ?>"/>
  </article>
  <article id="ARTecran">
    <p><?php echo json_decode($controllerPublic->getText("presentation", "ARTecran", "corps"), true)[0]["texte"]; ?></p>
    <img alt="image ecran" src="<?php echo json_decode($controllerPublic->getImg("presentation", "ARTecran"), true)[0]["link"]; ?>"/>
  </article>
  <article id="ARTsetlist">
    <p><?php echo json_decode($controllerPublic->getText("presentation", "ARTsetlist", "corps"), true)[0]["texte"]; ?></p>
    <img alt="banque morceau" src="<?php echo json_decode($controllerPublic->getImg("presentation", "ARTsetlist"), true)[0]["link"]; ?>"/>
  </article>
  <article id="ARTimportation">
    <p><?php echo json_decode($controllerPublic->getText("presentation", "ARTimportation", "corps"), true)[0]["texte"]; ?></p>
    <img alt="import morceau" src="<?php echo json_decode($controllerPublic->getImg("presentation", "ARTimportation"), true)[0]["link"]; ?>"/>
  </article>
</section>
