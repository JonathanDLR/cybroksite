<?php
require($_SERVER['DOCUMENT_ROOT'].'/jdlr/modefinder/controller/ControllerPublic.php');
$controllerPublic = new ControllerPublic();
?>
<section id="SECressource">
  <article id="ARTiphone">
    <p><?php echo json_decode($controllerPublic->getText("ressource", "ARTiphone", "corps"), true)[0]["texte"]; ?></p>
    <img alt="image iphone" src="<?php echo json_decode($controllerPublic->getImg("ressource", "ARTiphone"), true)[0]["link"]; ?>"/>
  </article>
  <article id="ARTipad">
    <p><?php echo json_decode($controllerPublic->getText("ressource", "ARTipad", "corps"), true)[0]["texte"]; ?></p>
    <img alt="image ipad" src="<?php echo json_decode($controllerPublic->getImg("ressource", "ARTipad"), true)[0]["link"]; ?>"/>
  </article>
  <article id="ARTitunes">
    <p><?php echo json_decode($controllerPublic->getText("ressource", "ARTitunes", "corps"), true)[0]["texte"]; ?></p>
    <img alt="image appstore" src="<?php echo json_decode($controllerPublic->getImg("ressource", "ARTitunes"), true)[0]["link"]; ?>"/>
  </article>
  <article id="ARTapi">
    <p><?php echo json_decode($controllerPublic->getText("ressource", "ARTapi", "corps"), true)[0]["texte"]; ?></p>
    <a href="http://api-cybrok-music.ovh/" target="_blank" id="apilink">
      <img alt="image api" id="apiimg" src="<?php echo json_decode($controllerPublic->getImg("ressource", "ARTapi"), true)[0]["link"]; ?>" />
    </a>
</section>
