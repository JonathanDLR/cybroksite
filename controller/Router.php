<?php 
require($_SERVER['DOCUMENT_ROOT'].'/jdlr/modefinder/controller/ControllerPublic.php');
require($_SERVER['DOCUMENT_ROOT'].'/jdlr/modefinder/controller/ControllerContact.php');
require($_SERVER['DOCUMENT_ROOT'].'/jdlr/modefinder/controller/ControllerCoAdmin.php');
require($_SERVER['DOCUMENT_ROOT'].'/jdlr/modefinder/controller/ControllerAdmin.php');
require($_SERVER['DOCUMENT_ROOT'].'/jdlr/modefinder/controller/ControllerImage.php');

class Router {
    private $_controllerPublic;
    private $_controllerContact;
    private $_controllerCoAdmin;
    private $_controllerAdmin;
    private $_controllerImage;

    public function __construct() {
        $this->_controllerPublic = new ControllerPublic();
        $this->_controllerContact = new ControllerContact();
        $this->_controllerCoAdmin = new ControllerCoAdmin();
        $this->_controllerAdmin = new ControllerAdmin();
        $this->_controllerImage = new ControllerImage();
    }
    
    public function public() {
        if ($_POST) {
            if (isset($_POST['content'])) {
              $this->_controllerContact->getContact();
            }
          } else {
          $this->_controllerPublic->getPage();
        }
    }

    public function admin() {
          if(isset($_SESSION['login']) && isset($_SESSION['password'])) {
            if(isset($_GET['action'])) {
              switch ($_GET['action']) {
                case 'deco':
                  $this->_controllerCoAdmin->deconnexion();
                default:
                  break;
              }
            }
            if(isset($_POST['getView'])) {
              $this->_controllerAdmin->getView();
            }
            else if(isset($_POST['nameView'])) {
              $this->_controllerAdmin->getArt();
            }
            else if(isset($_POST['nameArt'])) {
              $this->_controllerAdmin->getType();
            }
            else if(isset($_POST['nameType'])) {
              $this->_controllerAdmin->getText();
            }
            else if(isset($_POST['newText'])) {
              $this->_controllerAdmin->updateText();
            }
          
            else if(isset($_FILES['image'])) {
              $this->_controllerImage->send();
            }
            else {
              $this->_controllerCoAdmin->getAdmin();
            }
          } else if(isset($_POST['login'])) {
            $this->_controllerCoAdmin->sendAdmin();
          } else {
            $this->_controllerCoAdmin->getConnexion();
          }
    }
}




?>