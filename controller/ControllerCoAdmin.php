<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/jdlr/modefinder/backend/admin/AdminCoManager.php');

/**
 * CONTROLLER CONNEXION ADMIN
 */
class ControllerCoAdmin {
  private $_adminCoManager;

  public function __construct() {
    $this->_adminCoManager = new AdminCoManager();
  }

  function getConnexion() {
    include('frontend/common/headAdmin.html');
    include('frontend/common/header.php');
    include('frontend/view/admin/connexion.php');
    include('frontend/common/footerLegal.php');
    include('frontend/common/scriptAdmin.html');
  }

  function getAdmin() {
    include('frontend/common/headAdmin.html');
    include('frontend/common/header.php');
    include('frontend/view/admin/administration.php');
    include('frontend/common/footerLegal.php');
    include('frontend/common/scriptAdmin.html');
  }

  function deconnexion() {
    session_start();
    session_destroy();
  }

  function sendAdmin() {  // RECUP ET CONTROLE LOGIN MDP TAPE PAR USER

    if (isset($_POST['login'])) {
      $login =  htmlentities($_POST['login']); // HASH ET CONTROLE LOGIN MDP
      $password = htmlentities($_POST['password']);


      $reponse = $this->_adminCoManager->getAdmin(); // RECUP LOGIN MDP DE LA BDD
      $hashLogin = $reponse['loginname'];
      $hashPassword = $reponse['pswd'];

      if (empty($login)) { // SI LOGIN VIDE ON RENVOI VERS LA PAGE ERREUR
        echo 'Veuillez renseigner votre login!';
        header("refresh:2; URL=admin_page");
        return;
      }

      else {
        if (!password_verify($login, $hashLogin)) { // SI LOGIN ERRONE ON RENVOI VERS LA PAGE ERREUR
          echo 'Login invalide!';
          header("refresh:10; URL=admin_page");
          return;
        }
      }

      if (empty($password)) { // SI MDP VIDE ON RENVOI VERS LA PAGE ERREUR
        echo 'Veuillez renseigner votre mot de passe!';
        header("refresh:2; URL=admin_page");
        return;
      }

      else {
        if (!password_verify($password, $hashPassword)) { // SI MDP ERRONE ON RENVOI VERS LA PAGE ERREUR
          echo 'Mot de passe invalide!';
          header("refresh:2; URL=admin_page");
          return;
        }
      }

      session_cache_limiter('private'); // On passe le délai d'expiration de la session à 15 mn
      session_cache_expire(15);
      session_start(); // ON CREE UNE SESSION AVEC LOGIN ET MDP
      $_SESSION['login'] = $_POST['login'];
      $_SESSION ['password'] = $_POST['password'];
      header('Location: /admin_page');
      exit(); // RENVOI VERS LA PAGE ADMIN
    }
  }
}
?>
