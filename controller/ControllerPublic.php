<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/jdlr/modefinder/backend/admin/SendText.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/jdlr/modefinder/backend/admin/SendImg.php');

/**
 * CONTROLLER PAGE VITRINE
 */
class ControllerPublic {
  private $_sendText;
  private $_sendImg;

  public function __construct() {
    $this->_sendText = new SendText();
    $this->_sendImg = new SendImg();
  }

  public function getPage() {
    include('frontend/common/head.html');
    include('frontend/common/header.php');
    include('frontend/common/nav.php');
    include('frontend/common/menu.php');
    include('frontend/common/content.php');
    include('frontend/common/footer.php');
    include('frontend/common/script.html');
  }

  public function getText($nameView, $nameArt, $nameType) {
    $reponseText = $this->_sendText->send($nameView, $nameArt, $nameType);
    return $reponseText;
  }

  public function getImg($nameView, $nameArt) {
    $reponseImg = $this->_sendImg->send($nameView, $nameArt);
    return $reponseImg;
  }
}
?>
