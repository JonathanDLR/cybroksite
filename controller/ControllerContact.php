<?php
header('Content-Type: text/html; charset=utf-8');
require_once($_SERVER['DOCUMENT_ROOT'].'/jdlr/modefinder/backend/ContactManager.php');

/**
 * CONTROLLER PAGE CONTACT
 */
class ControllerContact
{
    private $_contactManager;

    public function __construct() {
        $this->_contactManager = new ContactManager();
    }

    public function correction($texte) // Securisation données users
    {
        $texte = htmlspecialchars($texte);
        $texte = stripslashes($texte);

        return $texte;
    }

    public function getContact()
    {
        if(isset($_POST)) {
            if(isset($_POST['nom']) && isset($_POST['mail']) && isset($_POST['object']) && isset($_POST['content'])) {
                $nom = $this->correction($_POST['nom']);
                $mail = $this->correction($_POST['mail']); 
                $object = $this->correction($_POST['object']);
                $content = $this->correction($_POST['content']);
        
                if (filter_var($mail, FILTER_VALIDATE_EMAIL)) {
                    $this->_contactManager->sendContact($nom, $mail, $object, $content); 
                }      
            }
        }
    }
}
?>