<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/jdlr/modefinder/backend/admin/SendView.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/jdlr/modefinder/backend/admin/SendArt.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/jdlr/modefinder/backend/admin/SendType.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/jdlr/modefinder/backend/admin/SendText.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/jdlr/modefinder/backend/admin/UpdateText.php');

/**
 * CONTROLLER PAGE ADMIN
 */
class ControllerAdmin {
  private $_sendView;
  private $_sendArt;
  private $_sendType;
  private $_sendText;
  private $_updateText;

  public function __construct() {
    $this->_sendView = new SendView();
    $this->_sendArt = new SendArt();
    $this->_sendType = new SendType();
    $this->_sendText = new SendText();
    $this->_updateText = new UpdateText();
  }

  function getView() { 
    $reponseView = $this->_sendView->send();

    echo $reponseView;
  }

  function getArt() {
    $nameView = $_POST['nameView'];
    $reponseArt = $this->_sendArt->send($nameView);

    echo $reponseArt;
  }

  function getType() {
    $nameView = $_POST['nameViewType'];
    $nameArt = $_POST['nameArt'];
    if ($nameArt == "") {
      $nameArt = NULL;
    }

    $reponseType = $this->_sendType->send($nameView, $nameArt);
    echo $reponseType;
  }

  function getText() {
    $nameView = $_POST['nameViewText'];
    $nameArt = $_POST['nameArtText'];
    if ($nameArt == "") {
      $nameArt = NULL;
    }
    $nameType = $_POST['nameType'];

    $reponseText = $this->_sendText->send($nameView, $nameArt, $nameType);
    echo $reponseText;
  }

  function updateText() {
    $newText = $_POST['newText'];
    $id = $_POST['id'];
    $this->_updateText->update($newText, $id);
  }
}

 ?>
