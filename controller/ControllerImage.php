<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/jdlr/modefinder/backend/admin/ImageManager.php');

/**
 * CONTROLLER ENVOI IMG
 */
class ControllerImage
{
    private $_imageManager;

    public function __construct() {
        $this->_imageManager = new ImageManager();
    }

    public function send()
    {
        $extensions = ['jpg', 'jpeg', 'png']; // Extensions autorisées
        $tempo = explode(".", $_FILES['image']['name']);
        $extensions_upload = end($tempo);
        $tmpname = $_FILES['image']['tmp_name'];
        if ($_POST['artImg'] == "null") {
            $target = "web/images/" . $_POST['viewImg'] . "_img." . $extensions_upload;
        }
        else {
            $target = "web/images/" . $_POST['viewImg'] . $_POST['artImg'] . "_img." . $extensions_upload;
        }
        if ($_FILES['image']['error'] > 0) {
            echo "error";
        }
        else {
            if(in_array($extensions_upload, $extensions)) { // Verif si extension envoyée ok
                $this->_imageManager->send($tmpname, $target, $_POST['viewImg'], $_POST['artImg']);
            }
        }
    }
}
?>