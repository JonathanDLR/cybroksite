<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/jdlr/modefinder/backend/Manager.php');

/**
 * RECUP DES TEXTES
 */
class TextAdminManager extends Manager {
  public function get($nameView, $nameArticle, $typeText) {
    $req = $this->_connexion->getDb()->prepare('SELECT texte.texte FROM texte JOIN vue
      ON texte.nameview_id = vue.view_id JOIN article ON texte.namearticle_id = article.article_id JOIN
      typetext ON texte.typetext_id = typetext.id WHERE vue.nameview = :nameView AND
      article.nameview = :nameArticle AND typetext.typetext = :typeText');
      $req->bindParam(':nameView', $nameView, PDO::PARAM_INT);
      $req->bindParam(':nameArticle', $nameArticle, PDO::PARAM_INT);
      $req->bindParam(':typeText', $typeText, PDO::PARAM_INT);
      $req->execute();
      $reponse = $req->fetch();

      echo $reponse;
  }
}
?>
