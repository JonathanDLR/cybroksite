<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/jdlr/modefinder/backend/Manager.php');

/**
 * RECUP NOM TYPE
 */
class SendType extends Manager {

  public function send($nameView, $nameArticle) {
    if($nameArticle == NULL) {
      $req = $this->_connexion->getDb()->prepare('SELECT typetext FROM (SELECT texte.texte,
        typetext.typetext, vue.nameview AS vue FROM texte JOIN typetext ON texte.typetext_id =
        typetext.id JOIN vue ON texte.nameview_id = vue.view_id) AS typetable  WHERE typetable.vue
        = :nameView');
        $req->bindParam(':nameView', $nameView, PDO::PARAM_STR);
        $req->execute();
        $arrayTypeText = $req->fetchAll(PDO::FETCH_ASSOC);
        $arrayTypeTextJson = json_encode($arrayTypeText);
    } else {
      $req = $this->_connexion->getDb()->prepare('SELECT typetext FROM (SELECT texte.texte,
        typetext.typetext, vue.nameview AS vue, article.nameart AS article FROM texte JOIN typetext
        ON texte.typetext_id = typetext.id JOIN vue ON texte.nameview_id = vue.view_id JOIN article
        ON texte.namearticle_id = article.article_id) AS typetable WHERE typetable.vue = :nameView AND
        typetable.article = :nameArticle');
        $req->bindParam(':nameView', $nameView, PDO::PARAM_STR);
        $req->bindParam(':nameArticle', $nameArticle, PDO::PARAM_STR);
        $req->execute();
        $arrayTypeText = $req->fetchAll(PDO::FETCH_ASSOC);
        $arrayTypeTextJson = json_encode($arrayTypeText);
    }

    return $arrayTypeTextJson;
  }
}
?>
