<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/jdlr/modefinder/backend/Manager.php');

/**
 * RECUP NOM VIEW
 */
class SendView extends Manager { 

  public function send() {
    $req = $this->_connexion->getDb()->prepare('SELECT nameview FROM vue');
    $req->execute();
    $arrayNameView = $req->fetchAll(PDO::FETCH_ASSOC);
    $arrayNameViewJson = json_encode($arrayNameView);

    return $arrayNameViewJson;
  }
}
?>
