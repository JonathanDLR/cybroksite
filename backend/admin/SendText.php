<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/jdlr/modefinder/backend/Manager.php');

/**
 * RECUP NOM TEXTE
 */
class SendText extends Manager { 

  public function send($nameView, $nameArticle, $nameType) {
    if($nameArticle == NULL) {
      $req = $this->_connexion->getDb()->prepare('SELECT texte, id FROM (SELECT texte.texte,
        texte.id, typetext.typetext AS typetext, vue.nameview AS vue FROM texte JOIN typetext
        ON texte.typetext_id = typetext.id JOIN vue ON texte.nameview_id = vue.view_id) AS typetable
        WHERE typetable.vue = :nameView AND typetable.typetext = :nameType');
        $req->bindParam(':nameView', $nameView, PDO::PARAM_STR);
        $req->bindParam(':nameType', $nameType, PDO::PARAM_STR);
        $req->execute();
        $arrayText = $req->fetchAll(PDO::FETCH_ASSOC);
        $arrayTextJson = json_encode($arrayText);
    } else {
      $req = $this->_connexion->getDb()->prepare('SELECT texte, id FROM (SELECT texte.texte,
        texte.id, typetext.typetext AS typetext, vue.nameview AS vue, article.nameart AS article
        FROM texte JOIN typetext ON texte.typetext_id = typetext.id JOIN vue ON texte.nameview_id =
        vue.view_id JOIN article ON texte.namearticle_id = article.article_id) AS typetable WHERE typetable.vue
        = :nameView AND typetable.article = :nameArticle AND typetable.typetext = :nameType');
        $req->bindParam(':nameView', $nameView, PDO::PARAM_STR);
        $req->bindParam(':nameArticle', $nameArticle, PDO::PARAM_STR);
        $req->bindParam(':nameType', $nameType, PDO::PARAM_STR);
        $req->execute();
        $arrayText = $req->fetchAll(PDO::FETCH_ASSOC);
        $arrayTextJson = json_encode($arrayText);
    }

    return $arrayTextJson;
  }
}
?>
