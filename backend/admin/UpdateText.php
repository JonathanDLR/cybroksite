<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/jdlr/modefinder/backend/Manager.php');

/**
 * UPDATE TEXTE DANS BDD
 */
class UpdateText extends Manager {

  public function update($newText, $id) {
    $req = $this->_connexion->getDb()->prepare('UPDATE texte SET texte = :newText WHERE id = :id');
    $req->bindParam(':newText', $newText, PDO::PARAM_STR);
    $req->bindParam(':id', $id, PDO::PARAM_INT);
    $req->execute();
  }
}
?>
