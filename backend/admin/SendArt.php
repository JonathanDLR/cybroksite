<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/jdlr/modefinder/backend/Manager.php');

/**
 * RECUP NOM ARTICLE
 */
class SendArt extends Manager { 

  public function send($nameView) {
    $req = $this->_connexion->getDb()->prepare('SELECT article.nameart FROM article JOIN
       vue on article.view_id = vue.view_id WHERE vue.nameview = :nameView');
    $req->bindParam(':nameView', $nameView, PDO::PARAM_STR);
    $req->execute();
    $arrayNameArt = $req->fetchAll(PDO::FETCH_ASSOC);
    $arrayNameArtJson = json_encode($arrayNameArt);

    return $arrayNameArtJson;
  }
}
?>
