<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/jdlr/modefinder/backend/Manager.php');

/**
 * RECUP LIENS IMG
 */
class SendImg extends Manager {

  public function send($nameView, $nameArticle) {
    if($nameArticle == NULL) {
      $req = $this->_connexion->getDb()->prepare('SELECT link, id FROM (SELECT image_link.link,
        image_link.id, vue.nameview AS vue FROM image_link JOIN vue ON image_link.view_id =
        vue.view_id) AS typetable WHERE typetable.vue = :nameView');
        $req->bindParam(':nameView', $nameView, PDO::PARAM_STR);
        $req->execute();
        $arrayText = $req->fetchAll(PDO::FETCH_ASSOC);
        $arrayTextJson = json_encode($arrayText);
    } else {
      $req = $this->_connexion->getDb()->prepare('SELECT link, id FROM (SELECT image_link.link,
        image_link.id, vue.nameview AS vue, article.nameart AS article FROM image_link JOIN vue
        ON image_link.view_id = vue.view_id JOIN article ON image_link.article_id =
        article.article_id) AS typetable WHERE typetable.vue = :nameView AND typetable.article =
        :nameArticle');
        $req->bindParam(':nameView', $nameView, PDO::PARAM_STR);
        $req->bindParam(':nameArticle', $nameArticle, PDO::PARAM_STR);
        $req->execute();
        $arrayText = $req->fetchAll(PDO::FETCH_ASSOC);
        $arrayTextJson = json_encode($arrayText);
    }

    return $arrayTextJson;
  }
}
?>