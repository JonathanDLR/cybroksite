<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/jdlr/modefinder/backend/Manager.php');

/**
 * RECUPERATION DU LOGIN ET DU MDP
 */
class AdminCoManager extends Manager {

  public function getAdmin() {
    $req = $this->_connexion->getDb()->prepare('SELECT loginname, pswd FROM membre');
    $req->execute();

    $reponse = $req->fetch(PDO::FETCH_ASSOC);

    return $reponse;
  }
}
?>
