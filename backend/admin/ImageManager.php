<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/jdlr/modefinder/backend/Manager.php');

/**
 * GESTIONNAIRE UPLOAD IMG
 */
class ImageManager extends Manager
{
    public function send($tmpname, $target, $viewImg, $artImg)
    {
        $result = move_uploaded_file($tmpname, $target); // Deplacement du fichier
        if ($result) {
            echo "Transfert Réussi!";
        }

        // Enregistrement du lien dans la bdd

        $req = $this->_connexion->getDb()->prepare('INSERT INTO image_link SET view_id = (SELECT
        vue.view_id FROM vue WHERE vue.nameview = :viewImg), article_id = (SELECT
        article.article_id FROM article WHERE article.nameart = :artImg), link = :link');
        $req->bindParam(':viewImg', $viewImg, PDO::PARAM_STR);
        $req->bindParam(':artImg', $artImg, PDO::PARAM_STR);
        $req->bindParam(':link', $target, PDO::PARAM_STR);
        $req->execute();
    } 
}
?>