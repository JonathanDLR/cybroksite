<?php
header('Content-Type: text/html; charset=utf-8');
require_once($_SERVER['DOCUMENT_ROOT'].'/jdlr/modefinder/backend/Manager.php');

/**
 * GESTIONNAIRE CONTACT
 */
class ContactManager extends Manager
{
    public function sendContact($nom, $mail, $object, $content)
    {
        // Enregistrement dans la BDD
        $req = $this->_connexion->getDb()->prepare('INSERT INTO contact (nom, mail, objet, content, date_envoi)
        VALUES (:nom, :mail, :object, :content, NOW())');
        $req->bindParam(':nom', $nom, PDO::PARAM_STR);
        $req->bindParam(':mail', $mail);
        $req->bindParam(':object', $object, PDO::PARAM_STR);
        $req->bindParam(':content', $content, PDO::PARAM_STR);
        $req->execute();

        // Formatage Mail
        $dest = 'jonathan.delarosa.dev@gmail.com';
        $content = "De ".$nom."\r\n"."\r\n".
                    $content."\r\n"."\r\n".
                    "Mail: ".$mail;

        $headers = 'Reply-To:'.$mail."\r\n".
                   'Content-Type: text/plain; charset="UTF-8"; DelSp="Yes"; format=flowed'."\r\n".
                   'Contet-Disposition: inline'."\r\n".
                   'Content-Transfert-Encoding: quoted-printable'."\r\n".
                   'MIME-VERSION: 1.0'."\r\n".
                   'X-Mailer/'.phpversion(); 

        $mail = mail($dest, $object, $content, $headers);

        if ($mail) {
            var_dump("toto");
        }
    }
}
?>