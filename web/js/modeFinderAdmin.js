/**
 * CHARGEMENT SCRIPT PAGE ADMIN
 */
INIT = {
  init: function() {
    DECONNEXION.init();
    MENU.init();
    GETVIEW.init();
  }
},

DECONNEXION = { // Deconnexion de la session
  init: function() {
    $("#BUTdeco").on('click', DECONNEXION.deco);
  },

  deco: function() {
    var deco = "deco";
    $.get(
      'admin.php',
      {
        action: deco
      },
      function success() {
        window.location.reload();
      }
    )
  }
},

GETVIEW = { // Alimentation du nom des vues dans le select
  init: function() {
    $("#BUTupdateArticle").on('click', GETVIEW.get);
    $("#BUTupdateImage").on('click', GETVIEW.get);
  },

  get: function() {
    var getView = "getView";
    $.post(
      'admin.php',
      {
        getView: "getView"
      },
      function success(data) {   
        $("#SELnameView").empty();  
        $("#SELnameView").off('change', GETART.get); 
        dataJson = $.parseJSON(data);
        for (var i in dataJson) {        
          $('<option>', {
            value: dataJson[i].nameview,
            id : "OPT".concat(dataJson[i].nameview)
          }).appendTo("#SELnameView");
          $("#OPT".concat(dataJson[i].nameview)).text(dataJson[i].nameview);
        }
        // Mise en place des events
        $("#SELnameView").on('change', GETART.get);
        $("#SELnameArt").on('change', GETTYPE.get);
        $("#SELnameType").on('change', GETTEXT.get);
        $("#BUTmodif").on('click', UPDATETEXT.upd);
        $("#formImage").on('submit', SENDIMAGE.sendImage);
        GETART.get(); // On charge l'init suivant
      }
    )
  }
},

GETART = { // Alimentation du nom des articles dans le select
  get: function() {
    $("#SELnameArt").empty();
    $.post(
      'admin.php',
      {
        nameView: $("#SELnameView option:selected").text() // ENVOI DE LA VUE SELECTIONNEE
      },
      function success(data) {
        dataJson = $.parseJSON(data);
        $("#SELnameArt").empty();
        for (var i in dataJson) { // CONSTRUCTION DE LELEM HTML
          $('<option>', {
            value: dataJson[i].name,
            id : "OPT".concat(dataJson[i].nameart)
          }).appendTo("#SELnameArt");
          $("#OPT".concat(dataJson[i].nameart)).text(dataJson[i].nameart);
        }
        GETTYPE.get(); // On charge l'init suivant
      }
    )
  }
},

GETTYPE = { // Alimentation du nom des types dans le select
  get: function() {
    $("#SELnameType").empty();
    $.post(
      'admin.php',
      {
        nameViewType: $("#SELnameView option:selected").text(),
        nameArt: $("#SELnameArt option:selected").text()
      },
      function success(data) {
        dataJson = $.parseJSON(data);
        $("#SELnameType").empty();
        // $("#SELnameType").off('change', GETTEXT.get);
        for (var key in dataJson) {
          value = dataJson[key];
          for (var i in value) {
            $('<option>', {
              value: value[i],
              id : "OPT".concat(value[i])
            }).appendTo("#SELnameType");
            $("#OPT".concat(value[i])).text(value[i]);
          }
        }
        GETTEXT.get(); // On charge l'init suivant
      }
    )
  }
},

GETTEXT = { // Alimentation des textes dans le div éditable
  get: function() {
    $("#ARTmodif div").empty();
    $.post(
      'admin.php',
      {
        nameViewText: $("#SELnameView option:selected").text(),
        nameArtText: $("#SELnameArt option:selected").text(),
        nameType: $("#SELnameType option:selected").text(),
      },
      function success(data) {
        dataJson = $.parseJSON(data);
        $("#ARTmodif div").empty();
        for (var key in dataJson) {
          value = dataJson[key];
          for (var i in value) {
            $("#ARTmodif DIV").text(value.texte);
            $("#ARTmodif DIV").attr("data-id", value.id);
          }
        }
      }
    )
  }
},

UPDATETEXT = { // Envoi des textes au php
  upd: function() {
    $.post(
      'admin.php',
      {
        newText: $("#ARTmodif DIV").text(),
        id: parseInt($("#ARTmodif DIV").attr("data-id"))
      },
      function success() {
        $("#ARTmodifRep").text("La modification a bien été effectuée!");
      }
    )
  }
}

window.onload = INIT.init();
