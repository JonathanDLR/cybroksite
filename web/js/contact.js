/**
 * ENVOI INFO CONTACT AU PHP
 */
CONTACT = {   
    sendcontact: function() {
        var regex = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        
        if($('#INPnom').val() == "") {
            alert('Veuillez renseigner votre nom');
        }
        else if($('#INPmail').val() == "") {
            alert('Veuillez renseigner votre mail');
        }
        else if($('#INPobject').val() == "") {
            alert('Veuillez renseigner votre sujet');
        }
        else if(($('#DIVmess').text() == "") || ($('#DIVmess').text() == "message")) {
            alert('Veuillez renseigner votre message');
        }
        else if(!regex.test($("#INPmail").val())) {
            alert('Votre email n\est pas valide');
        } else if($("#checkRGPD").is(":checked") == false) {
            alert("Vous n'avez pas cocher le message d'acceptation");
        }
        else {
            $.post(
                'index.php',
                {
                    nom: $('#INPnom').val(),
                    mail: $('#INPmail').val(),
                    object: $('#INPobject').val(),
                    content: $('#DIVmess').text()
                },
                function success() {
                    alert("Votre message a bien été envoyé");
                }
            )
        }
       
    },

    /**
     * GESTION DU MESSAGE RGPD
     */
    disclaimer: function() {
        if ($("#checkRGPD").is(":checked")) {
            $("#spanRGPD").text("");
        } else {
            $("#spanRGPD").text("Veuillez indiquer que vous accepter de nous transmettre vos coordonnées");
        }
    },
}

