/**
 * CHANGEMENT LABEL DU BOUTON DE SELECTION IMAGE
 */
BUTTONIMAGE = {
    filename: '',
    
    labelModif: function(e) {
        var labelVal = $("#LABimage");

        if (e.target.value) {
            BUTTONIMAGE.filename = e.target.value.split('\\').pop(); // On récupère et on formate le nom du fichier
        }
        
        if (BUTTONIMAGE.filename) {
            $("#LABimage").html(BUTTONIMAGE.filename); // on injecte le nom du fichier dans l'html
        }
        else {
            $("#LABimage").html(labelVal);
        }
    }
}
