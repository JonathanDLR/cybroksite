/**
 * PASSAGE EN FIX DE LA NAV
 */
NAVFIXED = {
	positionNav: $("nav").offset().top,

	init: function() {
		$(window).on("scroll", NAVFIXED.scrolledNav);
	},

	scrolledNav: function() {
		if ($(document).scrollTop() > NAVFIXED.positionNav) { // Si le haut du scroll dépasse la taille de la nav, on fixe
			$("nav").css("position", "fixed");
			$('nav').css("top", 0);
		}
		else {
			$("nav").css("position", "static");
		}
	}
}

window.onload = NAVFIXED.init();