/**
 * ANIMATION MENU VERSION MOBILE
 */
MENUMOB = { 
	init: function() {
		$('#DIVmenu').hide(); // ON CACHE LE MENU PAR DEFAUT
		$(window).resize(function() { // ON CACHE LE MENU AU REDIMENSIONNEMENT
			$('#DIVmenu').hide();
			$('.SECco').show(); // ON REFAIT APPARAITRE LE CONTENT
		});
		$('#menuBur').on('click', function() {
			MENUMOB.toggler();
			MENUMOB.hideContainer();
		});
	},

	 toggler: function() { // TOGGLE DU MENU
		$('#DIVmenu').toggle("slide", {direction: "left"}, 500);
	},

	hideContainer: function() { // TOGLE DU CONTAINER
		$('.SECco').toggle();
	}
}

window.onload = MENUMOB.init();