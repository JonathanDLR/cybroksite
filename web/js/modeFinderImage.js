/**
 * ENVOI DES IMAGES AU PHP
 */
SENDIMAGE = {
    sendImage: function(e) {
        e.preventDefault();
        var form = new FormData(this); // On créé un formulaire qui nous permets d'envoyer le fichier
        form.append("viewImg", $("#SELnameView").val());
        form.append("artImg", $("#SELnameArt").val());
        $.ajax({
            url: 'admin.php',
            type: "POST",
            data: form,
            cache: false,
            contentType: false,
            processData: false,
            success: function() {
                alert("Transfert réussi!");
            }
        })
    }
}