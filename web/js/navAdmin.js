/**
 * NAVIGATION VIA LES BOUTONS ADMIN
 */
NAVADMIN = {
    init: function () {
        $("#BUTupdateArticle").on('click', NAVADMIN.get);
        $("#BUTupdateImage").on('click', NAVADMIN.get);
    },

    get: function (e) {
        e.preventDefault();
        var urlId = "";

        if ($(this).attr('id') === "BUTupdateArticle") {
            $("#DIVadminImage").empty(); // On vide la vue adminImage
            urlId = "frontend/view/admin/adminUpdate.php";
            $("#DIVadminUpdate").load(urlId); // On charge en ajax la vue adminUpdate
        } else if ($(this).attr('id') === "BUTupdateImage") {
            $("#DIVadminUpdate").empty();
            urlId = "frontend/view/admin/adminImage.php";
            $("#DIVadminImage").load(urlId, function(responseText, textStatus, XMLHttpRequest) {
                $("#image").on('change', BUTTONIMAGE.labelModif); // Chargement de l'évènement de stylisation du bouton
            });          
        }
    }
}

window.onload = NAVADMIN.init();