/**
 * NAVIGATION VIA MENU
 */
MENU = {
  init: function() {
    $('nav a').on('click', MENU.navigate);
    $('#DIVmenu a').on('click', MENU.navigate);
    $('.SECcoPresentation a').on('click', MENU.navigate);
    $('#legal').on('click', MENU.navigate);
  },

  navigate: function(e) {
    e.preventDefault();
    var urlCible = $(this).attr('href');
    var urlId = $(this).attr('id');
    var divId = $('#DIV'.concat(urlId));
    $('.SECco').css('display', 'flex');
    $('.SECco > div').css('display', 'none');
    $('#DIVmenu').css('display', 'none');
    $(divId).css('display', 'flex');
    if(divId.text() == "") { // Chargement des pages en ajax si premier clic
      $(divId).load(urlCible, function(responseText, textStatus, XMLHttpRequest) {
        if(urlId === "contact") { // Chargement script envoi contact
          $('#BUTcont').on('click', CONTACT.sendcontact);
          $("#checkRGPD").on('click', CONTACT.disclaimer);
        }
      });     
    }
  }
}
