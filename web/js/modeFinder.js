/**
 * CHARGEMENT DIVERS SCRIPT AU CHARGEMENT DE LA PAGE
 */
INIT = {
  init: function() {
    LOADSITE.init();
    MENU.init();
  }
},

LOADSITE = { // Par défaut on charge la page présentation
  init: function() {
    $(".SECco").css("display", "flex");
    var url = "frontend/view/presentation.php";
    $("#DIVpresentation").load(url);
  }
}

window.onload = INIT.init();
