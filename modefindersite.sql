-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Lun 01 Avril 2019 à 09:41
-- Version du serveur :  10.1.37-MariaDB-0+deb9u1
-- Version de PHP :  7.3.3-1+0~20190307202245.32+stretch~1.gbp32ebb2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `modefindersite`
--

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

CREATE TABLE `article` (
  `article_id` int(10) UNSIGNED NOT NULL,
  `view_id` int(10) UNSIGNED DEFAULT NULL,
  `nameart` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `article`
--

INSERT INTO `article` (`article_id`, `view_id`, `nameart`) VALUES
(5, 3, 'ARTdescription'),
(6, 3, 'ARTreconnaissance'),
(7, 3, 'ARTecran'),
(8, 3, 'ARTsetlist'),
(9, 3, 'ARTimportation'),
(10, 4, 'ARTiphone'),
(11, 4, 'ARTipad'),
(12, 4, 'ARTitunes'),
(13, 7, 'ARTfacebook'),
(14, 7, 'ARTtwitter'),
(15, 4, 'ARTapi');

-- --------------------------------------------------------

--
-- Structure de la table `contact`
--

CREATE TABLE `contact` (
  `id` int(10) UNSIGNED NOT NULL,
  `nom` varchar(40) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `objet` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `date_envoi` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `contact`
--

INSERT INTO `contact` (`id`, `nom`, `mail`, `objet`, `content`, `date_envoi`) VALUES
(2, 'test', 'test@test.fr', 'test', 'test', '2018-11-29 15:22:41'),
(3, 'test', 'test@test.fr', 'test', 'test', '2019-02-16 18:16:07'),
(4, 'test', 'test@test.fr', 'test', 'test', '2019-02-16 18:17:08'),
(5, 'test', 'test@test.fr', 'test', 'test', '2019-02-16 18:17:41'),
(6, 'trt', 'delarosa.jonathan@free.fr', 'trtr', '', '2019-03-14 15:33:50'),
(7, 'test', 'delarosa.jonathan@free.fr', 'test', '', '2019-03-14 15:34:59'),
(8, 'test', 'delarosa.jonathan@free.fr', 'test', '', '2019-03-14 15:35:03'),
(9, 'test', 'delarosa.jonathan@free.fr', 'test', '', '2019-03-14 15:35:07');

-- --------------------------------------------------------

--
-- Structure de la table `image_link`
--

CREATE TABLE `image_link` (
  `id` int(10) UNSIGNED NOT NULL,
  `view_id` int(3) UNSIGNED NOT NULL,
  `article_id` int(3) UNSIGNED DEFAULT NULL,
  `link` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `image_link`
--

INSERT INTO `image_link` (`id`, `view_id`, `article_id`, `link`) VALUES
(7, 1, NULL, 'web/images/header_img.png'),
(13, 7, 13, 'web/images/footerARTfacebook_img.png'),
(14, 7, 14, 'web/images/footerARTtwitter_img.png'),
(41, 3, 5, 'web/images/presentationARTdescription_img.png'),
(42, 3, 6, 'web/images/presentationARTreconnaissance_img.png'),
(43, 3, 7, 'web/images/presentationARTecran_img.png'),
(44, 3, 8, 'web/images/presentationARTsetlist_img.png'),
(45, 3, 9, 'web/images/presentationARTimportation_img.png'),
(46, 4, 10, 'web/images/ressourceARTiphone_img.png'),
(47, 4, 11, 'web/images/ressourceARTipad_img.png'),
(48, 4, 12, 'web/images/ressourceARTitunes_img.png'),
(49, 4, 15, 'web/images/ressourceARTapi_img.png');

-- --------------------------------------------------------

--
-- Structure de la table `membre`
--

CREATE TABLE `membre` (
  `id` int(10) UNSIGNED NOT NULL,
  `loginname` varchar(255) NOT NULL,
  `pswd` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `membre`
--

INSERT INTO `membre` (`id`, `loginname`, `pswd`) VALUES
(1, '$2y$10$rVNdPSf4qgMEI37br9jiZeswkBVf4wPbWkr1U8npetJTxYmpkKYym', '$2y$10$rVNdPSf4qgMEI37br9jiZeswkBVf4wPbWkr1U8npetJTxYmpkKYym');

-- --------------------------------------------------------

--
-- Structure de la table `texte`
--

CREATE TABLE `texte` (
  `id` int(10) UNSIGNED NOT NULL,
  `nameview_id` int(3) UNSIGNED NOT NULL,
  `namearticle_id` int(3) UNSIGNED DEFAULT NULL,
  `typetext_id` int(10) UNSIGNED DEFAULT NULL,
  `texte` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `texte`
--

INSERT INTO `texte` (`id`, `nameview_id`, `namearticle_id`, `typetext_id`, `texte`) VALUES
(1, 1, NULL, 1, 'CYBROK'),
(2, 1, NULL, 2, 'The new app for learning music'),
(3, 2, NULL, 5, 'Presentation'),
(4, 2, NULL, 6, 'Ressources'),
(5, 2, NULL, 7, 'Contact'),
(6, 8, NULL, 3, 'L\'application qui vous aide à jouer en groupe!'),
(7, 3, 5, 3, 'CybRok is the new application helping you learning and practicing music. It will be with you for your curses, rehearsals and gigs!'),
(8, 3, 6, 3, 'CybRok can recognize the note, and calculate the bpm in real time! It write sheet music so you can see what you are playing.'),
(9, 3, 7, 3, 'With CybRok, you can share your phone screen with other devices. You can thus see the music sheet of other musician, and see what he\'s playing.'),
(10, 3, 8, 3, 'You can save setlists and several notes for your song. You\'ll never ever forget settings or which tunes you have to play.'),
(11, 3, 9, 3, 'CybRok can also analyze the song you have on your smartphone library.'),
(12, 4, 10, 3, 'Iphone 8: 4€99'),
(13, 4, 10, 8, 'Ipad Pro, ios 10'),
(14, 4, 11, 3, 'Ipad: 4€99'),
(15, 4, 11, 8, 'In app purchases ???'),
(16, 4, 12, 3, 'Download CybRok on Itunes.'),
(17, 6, NULL, 3, 'Copyright tout ça! Le droit c\'est important!'),
(18, 4, 15, 3, 'Save and share your songs notes with your band or your students with API Feature! 0.99€/mo.');

-- --------------------------------------------------------

--
-- Structure de la table `typetext`
--

CREATE TABLE `typetext` (
  `id` int(10) UNSIGNED NOT NULL,
  `typetext` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `typetext`
--

INSERT INTO `typetext` (`id`, `typetext`) VALUES
(1, 'titre'),
(2, 'soustitre'),
(3, 'corps'),
(4, 'article'),
(5, 'menu1'),
(6, 'menu2'),
(7, 'menu3'),
(8, 'corps2');

-- --------------------------------------------------------

--
-- Structure de la table `vue`
--

CREATE TABLE `vue` (
  `view_id` int(10) UNSIGNED NOT NULL,
  `nameview` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `vue`
--

INSERT INTO `vue` (`view_id`, `nameview`) VALUES
(1, 'header'),
(2, 'nav'),
(3, 'presentation'),
(4, 'ressource'),
(5, 'contact'),
(6, 'legal'),
(7, 'footer'),
(8, 'content');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`article_id`),
  ADD KEY `fk_article_view_id` (`view_id`);

--
-- Index pour la table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `image_link`
--
ALTER TABLE `image_link`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `link_unique` (`link`),
  ADD KEY `fk_image_view_id` (`view_id`),
  ADD KEY `fk_image_article_id` (`article_id`);

--
-- Index pour la table `membre`
--
ALTER TABLE `membre`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `texte`
--
ALTER TABLE `texte`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_texte_view_id` (`nameview_id`),
  ADD KEY `fk_texte_article_id` (`namearticle_id`),
  ADD KEY `fk_texte_typetext_id` (`typetext_id`);

--
-- Index pour la table `typetext`
--
ALTER TABLE `typetext`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `vue`
--
ALTER TABLE `vue`
  ADD PRIMARY KEY (`view_id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `article`
--
ALTER TABLE `article`
  MODIFY `article_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT pour la table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pour la table `image_link`
--
ALTER TABLE `image_link`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT pour la table `membre`
--
ALTER TABLE `membre`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `texte`
--
ALTER TABLE `texte`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT pour la table `typetext`
--
ALTER TABLE `typetext`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pour la table `vue`
--
ALTER TABLE `vue`
  MODIFY `view_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `fk_article_view_id` FOREIGN KEY (`view_id`) REFERENCES `vue` (`view_id`);

--
-- Contraintes pour la table `image_link`
--
ALTER TABLE `image_link`
  ADD CONSTRAINT `fk_image_article_id` FOREIGN KEY (`article_id`) REFERENCES `article` (`article_id`),
  ADD CONSTRAINT `fk_image_view_id` FOREIGN KEY (`view_id`) REFERENCES `vue` (`view_id`);

--
-- Contraintes pour la table `texte`
--
ALTER TABLE `texte`
  ADD CONSTRAINT `fk_texte_article_id` FOREIGN KEY (`namearticle_id`) REFERENCES `article` (`article_id`),
  ADD CONSTRAINT `fk_texte_typetext_id` FOREIGN KEY (`typetext_id`) REFERENCES `typetext` (`id`),
  ADD CONSTRAINT `fk_texte_view_id` FOREIGN KEY (`nameview_id`) REFERENCES `vue` (`view_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
